# Set up virtual environment

---

## Set up virtual environment

Create the python virtual environment
```bash
python3 -m venv local_venv
```

Activate the virtual environment
```bash
source local_venv/bin/activate
```

Install necessary python modules
```bash
pip3 install -r requirements_lda.txt
```

or

```bash
pip3 install -r requirements_nmf.txt
```


If you run into an issue while installing LightGBM, 
see the [Installation Guide](https://lightgbm.readthedocs.io/en/latest/Installation-Guide.html)

To deactivate a python virtual environment run the following command:
```bash
deactivate
```