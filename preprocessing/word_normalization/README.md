# Word Normalization

* Towards Data Science: [Text Normalization for Natural Language Processing (NLP)](https://towardsdatascience.com/text-normalization-for-natural-language-processing-nlp-70a314bfa646)
* Towards Data Science: [Text Normalization. Why, what, and how](https://towardsdatascience.com/text-normalization-7ecc8e084e31)
* [Tokenization and Text Normalization](https://www.analyticsvidhya.com/blog/2021/03/tokenization-and-text-normalization/)
* Medium: [Building a Stemmer](https://medium.com/analytics-vidhya/building-a-stemmer-492e9a128e84)
* [Multilingual search: Decompounding with language-specific lexicons](https://www.algolia.com/blog/engineering/increase-decompounding-accuracy-by-generating-a-language-specific-lexicon/)
* [Decompounding and Tokenization](https://www.litigationsupporttipofthenight.com/single-post/2019/05/19/decompounding-and-tokenization)
* [Sentencizer](https://spacy.io/api/sentencizer)

